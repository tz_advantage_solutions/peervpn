#!/bin/bash 

set -e
set -u
set -o pipefail

PROJECT_DIRECTORY=$1

VERSION="${2}"

mkdir -p ${PROJECT_DIRECTORY}/DEBIAN

cat <<EOF >${PROJECT_DIRECTORY}/DEBIAN/control
Package: peervpn
Version: ${VERSION}
Maintainer: Napad Maxim
Architecture: amd64
Section: non-free
Description: test package
EOF

dpkg-deb -z8 -Zgzip --build ${PROJECT_DIRECTORY}