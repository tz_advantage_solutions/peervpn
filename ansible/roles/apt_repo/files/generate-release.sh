#!/bin/bash

function do_packages(){
  cd $1
  apt-ftparchive packages . | gzip -9c > bionic/Packages.gz
}

OLD_PWD=${PWD}

do_packages $1

cd ${OLD_PWD}