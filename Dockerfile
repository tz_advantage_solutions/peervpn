FROM ubuntu:bionic

RUN apt-get update && \
    apt-get install -y build-essential git wget && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /work

WORKDIR /work
